package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/currantlabs/gatt"
	"github.com/currantlabs/gatt/examples/option"
	"github.com/spacemonkeygo/flagfile"
)

var (
	writeLocation = flag.String("file_location", "accel_data",
		"Location the file saves the UART data.")
)

var sig = make(chan os.Signal, 1)
var done = make(chan struct{})

func onStateChanged(d gatt.Device, s gatt.State) {
	fmt.Println("State:", s)
	switch s {
	case gatt.StatePoweredOn:
		fmt.Println("scanning...")
		d.Scan([]gatt.UUID{}, false)
		return
	default:
		d.StopScanning()
	}
}

func onPeripheralDiscovered(p gatt.Peripheral, a *gatt.Advertisement,
	rssi int) {
	if a.LocalName == "UART" {
		fmt.Printf("\nPeripheral ID:%s, NAME:(%s)\n", p.ID(), p.Name())
		fmt.Println("  Services          =")
		for _, service := range a.Services {
			fmt.Printf("\t%s\n", service.String())
		}
		fmt.Println("  Local Name        =", a.LocalName)
		fmt.Println("  TX Power Level    =", a.TxPowerLevel)
		fmt.Println("  Manufacturer Data =", a.ManufacturerData)
		fmt.Println("  Service Data      =", a.ServiceData)

		p.Device().Connect(p)
	}
}

func onPeripheralConnected(p gatt.Peripheral, err error) {
	// fmt.Printf("Connected to: %s\n", p.ID())
	defer p.Device().CancelConnection(p)

	services, err := p.DiscoverServices(nil)
	if err != nil {
		fmt.Printf("Failed to discover services, err: %s\n", err)
	}

	for _, service := range services {
		msg := "Service: " + service.UUID().String()
		if len(service.Name()) > 0 {
			msg += " (" + service.Name() + ")"
		}
		// fmt.Println(msg)

		characteristics, err := p.DiscoverCharacteristics(nil, service)
		if err != nil {
			fmt.Printf("Failed to discover characteristics, err: %s\n", err)
			continue
		}

		for _, char := range characteristics {
			msg := " Characteristics " + char.UUID().String()
			if len(char.Name()) > 0 {
				msg += " (" + char.Name() + ")"
			}
			msg += "\n   Properties    " + char.Properties().String()
			// fmt.Println(msg)

			_, err := p.DiscoverDescriptors(nil, char)
			if err != nil {
				fmt.Printf("Failed to discover Descriptors, err: %s\n", err)
				continue
			}

			// Read Characteristic if possible.
			if (char.Properties() & gatt.CharRead) != 0 {
				// TODO: write to file
				_, err := p.ReadCharacteristic(char)
				if err != nil {
					fmt.Printf("Failed to read characteristics, err: %s\n", err)
					continue
				}
				// fmt.Printf("     value      %x | %q\n", b, b)
			}

			// Subscribe the characteristic, sf possible.
			if (char.Properties() & (gatt.CharNotify | gatt.CharIndicate)) != 0 {
				fmt.Println("Connecting to Descriptors", char.Name())
				f := func(c *gatt.Characteristic, b []byte, err error) {
					err = writeFile(
						*writeLocation,
						fmt.Sprintf("[%s] %q\n", time.Now().UTC().Format("Mon Jan 2 15:04:05 -0700 MST 2006"), b))
					if err != nil {
						fmt.Printf("Error Writing to file: %s\n", err)
					}
				}
				if err := p.SetNotifyValue(char, f); err != nil {
					// fmt.Printf("Failed to subscribe characteristic, err: %s\n", err)
					continue
				}
			}
		}
		fmt.Println()
	}
	<-sig
}

func writeFile(fileName, data string) error {
	file, err := os.OpenFile(fileName,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		os.ModeAppend)

	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(data)
	if err != nil {
		return err
	}
	return nil
}

func onPeripheralDisconnected(p gatt.Peripheral, err error) {
	fmt.Println("Disconnected")
	close(done)
}

func main() {
	flagfile.Load()
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	d, err := gatt.NewDevice(option.DefaultClientOptions...)
	if err != nil {
		log.Fatalf("Failed to open device, err: %s\n", err)
		return
	}

	// Register handlers.
	d.Handle(
		gatt.PeripheralDiscovered(onPeripheralDiscovered),
		gatt.PeripheralConnected(onPeripheralConnected),
		gatt.PeripheralDisconnected(onPeripheralDisconnected))
	d.Init(onStateChanged)
	<-done
	writeFile(*writeLocation, "Application Closed")
	fmt.Println("Done")
}
